//
//  ViewController.swift
//  ParseDemo
//
//  Created by Guybrush_Treepwood on 01/10/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var messageTextField: UITextField!

    @IBOutlet weak var parseResults: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let  query = PFQuery(className: "Messages")
        
        
        query.orderByDescending("createdAt")
        query.findObjectsInBackgroundWithBlock { (results : [PFObject]?, error:NSError?) -> Void in
            if error == nil{
                for result in results! {
                    let message = result as PFObject
                    self.parseResults.text = self.parseResults.text + "\n" + (message["content"] as! String)
                }
            }
        }
      
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func sendMessage(sender: AnyObject) {
        let messageObject = PFObject(className: "Messages")
        messageObject["content"] = messageTextField.text
        messageObject.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            if success{
                self.messageTextField.text = ""
            }
            if error == nil{
                
            }
        }

    }


}

