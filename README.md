#Swift iOS Course: iOS 8 Apps for every Screen Size


![edx-microsoft-logo.jpg](https://d13yacurqjgara.cloudfront.net/users/464600/screenshots/1566274/dribble800x600.gif)


#Description 

I this course we are going to have a look at the different screen sizes, as well as the many devices out there. And there are quite a lot of them. So we need to ask our selves one question: What does ‚truly universal‘ actually mean?

We are going to start by taking a look at Auto Layout and especially Size Classes, which were introduced with iOS 8. Using these features will enable you to create user interfaces that adjust themselves to any rotation and screen size.

We will continue with learning about techniques to identify the currently used device so that you can customize the user interfaces for each device type and thereby provide your users with the best experience possible.

Once your users have installed your App on more than one device you will have to think about synchronizing data. Therefore you will learn a lot about Parse, a backend service with awesome cloud features.

After learning the basics of creating universal user interfaces we are going to build a real world application from scratch. By creating an instagram like app you can put into practice everything you have learned so far.




#Goals 

Create universal Apps that run on all iPhone sizes, iPad and iPod Touch

Using Auto Layout to create user interfaces that will adapt themselves according to the device orientation and screen size

Using Size Classes to customize the layout for specific devices like the iPhone 6 Plus

Influence UI changes while rotating a device

Integrate Parse into every iOS App

Save and retrieve Data from Parse
