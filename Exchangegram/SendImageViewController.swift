//
//  SendImageViewController.swift
//  Exchangegram
//
//  Created by Guybrush_Treepwood on 01/10/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

import UIKit

class SendImageViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var messageTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let recognizer = UITapGestureRecognizer(target: self, action: "displayImagePicker:")
        recognizer.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(recognizer)
        
    }
    
    func displayImagePicker ( recog: UITapGestureRecognizer){
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var progressBar: UIProgressView!
    
    
    @IBAction func sendImage(sender: AnyObject) {
        
        let message = PFObject (className: "Message")
        message["textContent"] = messageTextView.text
        
        let imageData = UIImageJPEGRepresentation(imageView.image!, 0.5)
        let messageImage = PFFile(data: imageData!)
        
        messageImage.saveInBackgroundWithBlock({ (sucess:Bool, error:NSError?) -> Void in
            
            if sucess{
                message["messageImage"] = messageImage
                message.saveInBackground()
                self.dismissViewControllerAnimated(true, completion: nil)
            }
            
            }) { (progress:Int32) -> Void in
                let progressF : Float = (Float(progress)/100)
                self.progressBar.setProgress(progressF, animated: true)
                
        }
        
        
    }
    
    
    @IBAction func dismissViewController(sender: AnyObject) {
        self.dismissViewControllerAnimated(true , completion: nil)
    }

    
    // MARK: Image Picker Controller Delegate
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        imageView.contentMode = UIViewContentMode.ScaleAspectFill
        imageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        picker.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
