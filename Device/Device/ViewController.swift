//
//  ViewController.swift
//  Device
//
//  Created by Guybrush_Treepwood on 01/10/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
            label.text = "iPad"
        }else{
            var detectionSize : CGFloat = 0

            if self.interfaceOrientation == .Portrait{
                detectionSize = self.view.bounds.height
            }else{
                detectionSize = self.view.bounds.width

            }
           
            
            switch detectionSize{
            case 480:
                label.text = "This iPhone 4S"
            case 568:
                label.text = "This iPhone  5/5S"
            case 667:
                label.text = "This iPhone 6"
            case 736:
                label.text = "This iPhone 6 Plus"
            default:
                label.text = "This iPhone "

            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

