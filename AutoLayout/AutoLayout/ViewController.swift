//
//  ViewController.swift
//  AutoLayout
//
//  Created by Guybrush_Treepwood on 01/10/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        UIView.animateWithDuration(0.2) { () -> Void in
            self.textField.alpha = 1
        }
        switch fromInterfaceOrientation{
        case .Portrait:
            textField.text = "Rotated from Portrait"
        case.LandscapeLeft:
            textField.text = "Rotate From Landscape"
        case.LandscapeRight:
            textField.text = "Rotate From Landscape"
        default:
            textField.text = "Rotate"
        }
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        UIView.animateWithDuration(duration) { () -> Void in
            self.textField.alpha = 0
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

